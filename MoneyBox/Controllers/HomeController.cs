﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MoneyBox.Controllers
{
    public class HomeController : Controller
    {
        static int moneyBox = 0;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [HttpPost]
        public JsonResult AddCoin()
        {
            moneyBox += 1;
            return Json(moneyBox.ToString());
        }

        [HttpGet]
        public JsonResult GetMoneyBox()
        {
            return Json(moneyBox.ToString());
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}